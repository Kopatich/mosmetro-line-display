import React from 'react';
import styled from 'styled-components';
import { baseMarkHeight, baseMarkWidth, innerMarkHeight, innerMarkWidth } from '../../../constants/markings';

interface StationLineMarkProps {
    color: string;
    isActive?: boolean;
    className?: string
}

type StyledContainerProps = Pick<StationLineMarkProps, 'color' | 'isActive' | 'className'>;

const StyledContainer = styled.div<StyledContainerProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    border-radius: 50%;

    width: ${baseMarkWidth}px;
    height: ${baseMarkHeight}px;
    background-color: ${({color}) => color};
`;

const StyledInnerContainer = styled.div<StyledContainerProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    border-radius: 50%;

    width: ${innerMarkWidth}px;
    height: ${innerMarkHeight}px;
    background-color: ${({color}) =>( color==='') ? 'transparent': 'white'};
`;

const StationLineMark: React.FC<StationLineMarkProps> = ({ className, color, isActive = false }) => {
	return (
		<StyledContainer className={className} color={color} isActive={isActive}>
			<StyledInnerContainer color={color} />
		</StyledContainer>
	);
};

export default StationLineMark;