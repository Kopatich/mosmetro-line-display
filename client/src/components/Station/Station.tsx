import React from 'react';
import styled from 'styled-components';
import { baseMarkWidth, innerMarkWidth } from '../../constants/markings';
import { getTextWidth } from '../../tools/getTextWidth';
import Divider from '../Divider/Divider';
import StationLineMark from './StationLineMark/StationLineMark';

const StyledContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;


`;

const StyledLineContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

interface StyledStationLineMark {
    className?: string;
}

const StyledStationLineMark = styled(StationLineMark)<StyledStationLineMark>`
    margin-inline: -5px;

    &:first-child {
        margin-left: 0;
    }
`;

const StyledStationName = styled.span`
    margin-bottom: 4px;
    margin-left: ${(baseMarkWidth - innerMarkWidth)/2}px;
`;

interface StationProps {
    name: string;
    color: string;
    isDividerDrawn?: boolean;
}

const Station: React.FC<StationProps> = ({ name, color, isDividerDrawn = true }) => {
	const className = React.useId();
	const stationNameRef = React.createRef<HTMLSpanElement>();

	return (
		<StyledContainer>
			<StyledStationName ref={stationNameRef}>{name}</StyledStationName>
			<StyledLineContainer>
				<StyledStationLineMark color={/[а-я0-9]/i.test(name) ? color : ''} className={className} />
				{isDividerDrawn ? <Divider offset={getTextWidth(name, 'normal 16pt \'Moscow Sans\'')} color={color} /> : null}
			</StyledLineContainer>
		</StyledContainer>
	);
};

export default Station;