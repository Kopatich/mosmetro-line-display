import React from 'react';
import styled, { css } from 'styled-components';
import { baseMarkWidth, innerMarkHeight } from '../../constants/markings';

const nextStationPadding = baseMarkWidth + 10;

type StyledDividerProps = Pick<DividerProps, 'offset' | 'color'>;

const StyledDivider = styled.div<StyledDividerProps>`
    ${({ offset}) => { 
		return css`
            width: ${(offset ?? 0) - baseMarkWidth + nextStationPadding}px;
            `;
	}}
    
    height: ${innerMarkHeight}px;
    background-color: ${({ color }) => color};
    
    margin-inline: ${-baseMarkWidth / 2}px;
    z-index: -1;
`;

interface DividerProps {
    offset: number;
    color: string;
}

const Divider: React.FC<DividerProps> = ({ offset, color }) => {
	return (
		<StyledDivider color={color} offset={offset} />
	);
};

export default Divider;