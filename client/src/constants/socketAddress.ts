const host = 'localhost';
const port = '3001';

export default `http://${host}:${port}`;