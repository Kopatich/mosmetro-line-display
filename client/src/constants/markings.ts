export const baseMarkWidth = 25;
export const baseMarkHeight = 25;

export const innerMarkWidth = 13;
export const innerMarkHeight = 13;