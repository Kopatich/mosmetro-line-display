import React from 'react';
import { io } from 'socket.io-client';
import styled from 'styled-components';
import './App.css';
import Station from './components/Station/Station';
import { baseMarkWidth } from './constants/markings';

const socket = io('http://localhost:3001');
const room = 1;
const line = 1;

const StyledContainer = styled.div`
	display: flex;
	flex-direction: row;
	margin-left: ${-(baseMarkWidth/2)}px;
`;

interface Line {
	number: number;
	color: string;
	stations: {
		name: string,
		isLast: boolean,
	}[];
}

function App() {
	const [lineData, setLineData] = React.useState<Line>({number: 1, color: '', stations: []});

	React.useEffect(() => {
		socket.on('line', (data) => {
			setLineData(data);
		});
	}, [socket]);

	return (
		<>
			<button onClick={() => socket.emit('join_room', room)}>
				Join room {room}
			</button>
			<button onClick={() => socket.emit('getLine', { room, line })}>
				Get line {line}
			</button>
			<StyledContainer>
				<Station key={-1} name={Array(3).fill(' ').join('')} color={lineData.color} isDividerDrawn />
				{lineData.stations.map((station, index) => (
					<Station
						key={index}
						name={station.name}
						color={lineData.color}
						isDividerDrawn={index !== lineData.stations.length - 1}
					/>))}
			</StyledContainer>
		</>
	);
}

export default App;
