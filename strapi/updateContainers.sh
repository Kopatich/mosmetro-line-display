#!/bin/bash

STRAPI_CONTAINER="registry.gitlab.com/kopatich/mosmetro-line-display/strapi:latest"
STRAPI_DB_CONTAINER="registry.gitlab.com/kopatich/mosmetro-line-display/strapi_db:latest"

STRAPI_CONTAINER_ID=$(docker ps --no-trunc -aqf name="^*strapi*$")
STRAPI_DB_CONTAINER_ID=$(docker ps --no-trunc -aqf name="^*strapiDB*$")

docker commit $STRAPI_CONTAINER_ID $STRAPI_CONTAINER
echo "[COMMIT] $STRAPI_CONTAINER_ID: $STRAPI_CONTAINER"
docker commit $STRAPI_DB_CONTAINER_ID $STRAPI_DB_CONTAINER
echo "[COMMIT] $STRAPI_DB_CONTAINER_ID: $STRAPI_DB_CONTAINER"

docker push $STRAPI_CONTAINER
echo "[PUSH] $STRAPI_CONTAINER_ID: $STRAPI_CONTAINER"
docker push $STRAPI_DB_CONTAINER
echo "[PUSH] $STRAPI_DB_CONTAINER_ID: $STRAPI_DB_CONTAINER"

