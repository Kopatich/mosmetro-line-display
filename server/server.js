const express = require('express');
const app = express();
const http = require('http');
const { Server } = require('socket.io');
const cors = require('cors');
const fetch = require('node-fetch');

const STRAPI = {
	HOST: 'localhost',
	PORT: 1337
};
const jwt = '283ee434cf1ca2fb3ada61a84682881240e3973e2a4d55aaf6ece0fdb429050f906b18d1a5de3443d41f0a360bf68b077ed14a808e61c275c9a6442a6f5f73fdbbfcf6390c032cd91bab39a10c80b56cea562787ad8ba62809af2d1e016082c354b03b90c08ad0f3bf03ed9a24ec9c6f245187a6a00e841232e45e261f3fe477';

app.use(cors());

const server = http.createServer(app);

const io = new Server(server, {
	cors: {
		origin: 'http://localhost:3000',
		methods: ['GET', 'POST'],
	},
});

io.on('connection', (socket) => {
	console.log(`User Connected: ${socket.id}`);

	socket.on('disconnect', () => {
		// EDIT: временно стоит, потом надо заменить
		socket.leave(1);
		console.log(`UserId ${socket.id} disconnected`);
	});

	socket.on('join_room', (data) => {
		socket.join(data);
		console.log(`${socket.id} joined room: ${data}`);
	});

	socket.on('getLine', (data) => {
		console.log(`[SOCKET REQUEST] getLine: ${JSON.stringify(data, null, 4)}`);

		fetch(`http://${STRAPI.HOST}:${STRAPI.PORT}/api/lines/${data.line}?populate=%2A`, {
			headers: {
				Authorization: `Bearer ${jwt}`
			}
		})
			.then(response => response.json())
			.then(response => response.data.attributes)
			.then(response => {
				const { color, number, stations } = response;

				socket.emit('line',
					{
						color,
						number,
						stations: stations.data.map(({ attributes }) => ({ name: attributes.name, isLast: attributes.isLast })),
					});
			})
			.catch(e => console.log(e));
	});
});

server.listen(3001, () => {
	console.log('SERVER IS RUNNING');
});
